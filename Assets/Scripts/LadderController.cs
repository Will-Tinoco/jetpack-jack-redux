﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderController : MonoBehaviour
{
    // Is this ladder a pole? If yes, the player will be able to rotate around it; if no, they can only climb one side
    public bool IsPole;
    // Radius of the pole if this is one (unused otherwise)
    public float PoleRadius = 0.25f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        GameObject _OtherObject = other.gameObject;
        if (_OtherObject.tag == "Player")
        {
            PlayerController _Player = _OtherObject.GetComponent<PlayerController>();
            if (_Player != null && _Player.CanMountLadder())
            {
                if (IsPole)
                {
                    _Player.ChangeMode_Climbing_Pole(new Vector3(0, Vector3.Angle(_OtherObject.transform.position, transform.position), 0), transform.position, PoleRadius);
                    //print("Player is now climbing a pole");
                }
                else
                {
                    _Player.ChangeMode_Climbing_Ladder(transform.forward, transform.position);
                    //print("Player is now climbing a ladder");
                }
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        GameObject _OtherObject = other.gameObject;
        if (_OtherObject.tag == "Player")
        {
            PlayerController _Player = _OtherObject.GetComponent<PlayerController>();
            if (_Player != null)
            {
                //_Player.DismountLadder();
                _Player.ChangeMode_Grounded();
                //print("Player left ladder");
            }
        }
    }
}
