﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTriggerController_Surface : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            PlayerController _Player = other.GetComponent<PlayerController>();
            _Player.TouchingWater_Surface = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            PlayerController _Player = other.GetComponent<PlayerController>();
            _Player.TouchingWater_Surface = false;
        }
    }
}
