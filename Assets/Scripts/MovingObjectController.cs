﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObjectController : MonoBehaviour {
    public Vector3 MoveDistance;
    //Vector3 StartPos;
    public float MoveTime = 1.0f;
    public bool MoveForever;
    //bool MovingBack;
    public float StopTime = 1.0f;
    public Vector3 RotationDistance;
    public float RotationTime = 1.0f;
    public bool RotateForever;
    public bool BounceRotation;
    public float RotationStopTime = 1.0f;
    public bool RotateAroundPoint;
    public Vector3 RotationOffset;
    Vector3 RotationPoint;

    // Use this for initialization
    void Start () {
        //StartPos = transform.position;
        if (!RotateAroundPoint)
        {
            RotationPoint = transform.position;
        }
        else
        {
            RotationPoint = transform.position + RotationOffset;
        }

        StartCoroutine(MoveForward());
        StartCoroutine(RotateForward());
	}
	
	// Update is called once per frame
	void Update () {

    }

    private void FixedUpdate()
    {

    }

    IEnumerator MoveForward()
    {
        if (MoveForever)
        {
            while (true)
            {
                transform.position += MoveDistance * Time.deltaTime * (1 / MoveTime);
                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            float i = 0;
            while (i < MoveTime)
            {
                transform.position += MoveDistance * Time.deltaTime * (1 / MoveTime);
                i += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForSeconds(StopTime);
            StartCoroutine(MoveBackward());
        }
    }

    IEnumerator MoveBackward()
    {
        float i = 0;
        while (i < MoveTime)
        {
            transform.position -= MoveDistance * Time.deltaTime * (1 / MoveTime);
            i += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(StopTime);
        StartCoroutine(MoveForward());
    }

    IEnumerator RotateForward()
    {
        if (RotateForever)
        {
            while (true)
            {
                transform.RotateAround(RotationPoint, Vector3.right, RotationDistance.x * Time.deltaTime * (1 / RotationTime));
                transform.RotateAround(RotationPoint, Vector3.up, RotationDistance.y * Time.deltaTime * (1 / RotationTime));
                transform.RotateAround(RotationPoint, Vector3.forward, RotationDistance.z * Time.deltaTime * (1 / RotationTime));
                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            float i = 0;
            while(i < RotationTime)
            {
                transform.RotateAround(RotationPoint, Vector3.right, RotationDistance.x * Time.deltaTime * (1 / RotationTime));
                transform.RotateAround(RotationPoint, Vector3.up, RotationDistance.y * Time.deltaTime * (1 / RotationTime));
                transform.RotateAround(RotationPoint, Vector3.forward, RotationDistance.z * Time.deltaTime * (1 / RotationTime));
                i += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForSeconds(RotationStopTime);
            StartCoroutine(RotateBackward());
        }
    }

    IEnumerator RotateBackward()
    {
        if (BounceRotation)
        {
            if (RotateForever)
            {
                while (true)
                {

                    transform.RotateAround(RotationPoint, Vector3.right, -RotationDistance.x * Time.deltaTime * (1 / RotationTime));
                    transform.RotateAround(RotationPoint, Vector3.up, -RotationDistance.y * Time.deltaTime * (1 / RotationTime));
                    transform.RotateAround(RotationPoint, Vector3.forward, -RotationDistance.z * Time.deltaTime * (1 / RotationTime));
                    yield return new WaitForEndOfFrame();
                }
            }
            else
            {
                float i = 0;
                while (i < RotationTime)
                {
                    transform.RotateAround(RotationPoint, Vector3.right, -RotationDistance.x * Time.deltaTime * (1 / RotationTime));
                    transform.RotateAround(RotationPoint, Vector3.up, -RotationDistance.y * Time.deltaTime * (1 / RotationTime));
                    transform.RotateAround(RotationPoint, Vector3.forward, -RotationDistance.z * Time.deltaTime * (1 / RotationTime));
                    i += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
                yield return new WaitForSeconds(RotationStopTime);
                StartCoroutine(RotateBackward());
            }
        }
        else
        {
            StartCoroutine(RotateForward());
        }
    }
}
