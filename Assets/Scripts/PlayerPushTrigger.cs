﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPushTrigger : MonoBehaviour {
    bool Active = true;
    public Vector3 PushDirection;
    public float PushForce = 1.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay(Collider other)
    {
        GameObject _other = other.gameObject;

        if(Active && _other.tag == "Player")
        {
            _other.GetComponent<Rigidbody>().AddForce(PushDirection.normalized * PushForce * Time.fixedDeltaTime, ForceMode.Acceleration);
        }
    }

    public void Activate()
    {
        Active = true;
    }

    public void Deactivate()
    {
        Active = false;
    }
}
