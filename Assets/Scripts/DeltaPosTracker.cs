﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeltaPosTracker : MonoBehaviour {
    //public bool MakePlayerFollow;
    //public bool PlayerMatchRotation;
    //PlayerController Player;
    Vector3 PosLastFrame;
    Vector3 DeltaPos;
    
    Vector3 DeltaRotationTotal;
    float DeltaRotation;
    Quaternion RotationLastFrame;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        DeltaPos = transform.position - PosLastFrame;
        //print("Position last frame was " + PosLastFrame);
        //print("Position this frame is " + transform.position);
        //print("Movement this frame was " + DeltaPos.ToString("F4"));
        PosLastFrame = transform.position;

        DeltaRotation = Quaternion.Angle(transform.rotation, RotationLastFrame);
        //print("Rotation this frame was " + DeltaRotation.ToString("F4"));
        RotationLastFrame = transform.rotation;
    }

    public Vector3 GetDeltaPos()
    {
        return DeltaPos;
    }

    public float GetDeltaRotation()
    {
        return DeltaRotation;
    }
}
