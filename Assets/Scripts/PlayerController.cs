﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    enum MoveMode { Grounded, Flying, Climbing_Ladder, Climbing_Pole, Swimming_Surface, Swimming_Deep };

    // Player's Rigidbody component
    Rigidbody rigidBody;
    // Current movement vector (reset every frame, values determined by player input + factoring in walls and ground normal in functions)
    Vector3 MovementVector;
    // Raw movement vector with no modifiers (used for pole-climbing)
    Vector3 MovementVector_Raw;
    // Additional movement to be applied (used for moving/rotating platforms)
    Vector3 ExtraMovement;
    // Non-rotated movement vector (used to add a subtle rotation to the game camera while flying based on movement direction)
    Vector3 MovementVector_WorldAxes;
    // Position of a ladder the player is climbing
    Vector3 LadderPosition;
    // Radius of the pole being climbed
    float PoleRadius;
    // Position relative to a pole while climbing it
    Vector3 PoleOffset;

    // ---

    // Forward-direction reference object (usually the CamParent, which directs the camera)
    public Transform ForwardRef;
    // Minimum amount of joystick tilt to register movement (prevents movement when joystick is only tilted slightly - also good for broken joysticks)
    public float ControllerDeadZone = 0.3f;
    // Player movement speed
    public float MoveSpeed = 12.0f;
    // [UNUSED: Not sure if this is used anymore?]
    // Rate at which the player accelerates to full speed when beginning to move from a standstill
    // public float AccelerationSpeed = 4.0f;
    // Climbing speed for ladders and poles
    public float ClimbSpeed = 0.8f;
    // rotation speed for climbing poles
    public float PoleRotationSpeed = 5.0f;
    // Jump strength
    public float JumpPower = 10.0f;
    // Late jump timer
    public float LateJumpDelay = 0.2f;
    // Maximum ground slope
    public float MaxSlope = 50.0f;
    // Drag when in the air - closer to 1 means less drag
    public float AerialDrag = 0.95f;
    // Drag when in water
    public float WaterDrag = 0.99f;
    // Rate of descent while flying
    public float FlightDescentSpeed = 0.5f;
    // Rate of descent while gliding
    public float GlideDescentSpeed = 0.5f;
    // Jump power while flying (jumping while flying creates a small burst of upward momentum)
    public float JumpPower_Flying = 5.0f;
    // Rotation sensitivity when flying using the mouse
    public float FlyingRotationSensitivity_Mouse = 1.0f;
    // Rotation sensitivity when flying using a gamepad
    public float FlyingRotationSensitivity_Joystick = 1.0f;
    // Time the player can fall before taking fall damage on impact with a solid surface
    public float FallDamageTime = 1.5f;
    // [OBSOLETE: Now handled by whether the player will take fall damage or not on hitting the water]
    // Maximum fall speed to end up floating on the water's surface when falling
    public float MaxSpeedForWaterFloat = -5.0f;
    // Maximum time the player may glide after a single jump
    public float MaxGlideTime = 1.5f;

    // ---

    // Toggle receiving player input on/off (useful for cutscenes and the like)
    bool ControlsEnabled = true;
    // Check whether the player is currently grounded (to determine if they can jump, should factor in ground normals for movement, etc.)
    bool Grounded = false;
    // Check whether player is jumping (to avoid accidentally snapping to the ground when trying to jump, and a few other similar scenarios)
    bool Jumping = false;
    // [OBSOLETE: Replaced by LateJumpTimerRunning below]
    // Check if late jump timer is running
    bool LateJumpTimerActive = false;
    // Check if the player can double-jump (un-checked when the player double-jumps, reset when the player is grounded)
    bool CanDoubleJump = true;
    // [UNUSED: Ledge-grabbing functions are currently not called due to inconsistent behavior, so ledge-grabbing is not present as of now]
    // Check if the player is currently hanging from a ledge
    bool GrabbingLedge = false;
    // Check if the player is currently crouching (crouch state is used to charge certain attacks and slows player movement)
    bool Crouching = false;
    // If the player falls for long enough, this flag is set to cause them to take fall damage upon landing, unless they slow down / jump again / otherwise stop their fall before hitting the ground
    bool FallDamage = false;
    // Check if the player is actively gliding
    bool InGlide = false;
    // Check if the player has already finished their glide for the current jump (set as soon as the player glides for longer than the max glide duration, reset upon landing)
    bool GlideOver = false;
    // Check whether the player is being told not to grab a ladder/pole (used when the player jumps off of a ladder/pole so they don't immediately re-grab it)
    bool OverrideLadderGrab = false;
    // Check if the player is currently touching the surface of a water body
    [HideInInspector]
    public bool TouchingWater_Surface = false;
    // Check if the player is currently touching the deeper part of a water body
    [HideInInspector]
    public bool TouchingWater_Deep = false;

    // ---

    // Current movement mode (determines how input is read/used)
    MoveMode Mode = MoveMode.Grounded; 
    // Ground normal (used for altering movement to match ground slope)
    Vector3 GroundNormal = Vector3.up;
    // Wall normal (used to keep the player from being pushed into walls when moving)
    Vector3 WallNormal = new Vector3();
    // Check if late-jump timer is currently running
    Coroutine LateJumpTimerRunning;
    // [UNUSED: Punch attack is not currently implemented]
    // Check if the player is currently in the middle of a punch attack
    Coroutine AttackActive_Punch;
    // Check if the player is currently in the middle of a roll attack
    Coroutine AttackActive_Roll;
    // Check if the player is currently falling (used to determine when the player will take fall damage)
    Coroutine Falling;
    // Check if the player is currently gliding after a double-jump
    Coroutine Gliding;
    // Movement vector for when the player continues moving in a set direction regardless of current input (used for roll attack)
    Vector3 SavedMovement;
    // Object on which the player is currently standing (used to find moving platforms and move along with them)
    DeltaPosTracker PlatformDelta;
    // Position tracker for the player
    DeltaPosTracker MyPos;
    
    // ---

    // [NOTE: These layermasks are used by the wall/ground-finding functions to ignore water bodies. Otherwise the player can walk on / be pushed away from water.)
    // Layermask that looks only for water layers
    LayerMask WaterMask;
    // Layermask that looks only for the water's surface
    LayerMask WaterMask_Surface;
    // Layermask that looks only for the deep part of a water body
    // [NOTE: This layermask is also used for keeping the player on the water's surface when surface-swimming by finding the top of the deep part of the water body
    // and preserving the player's Y position relative to it.)
    LayerMask WaterMask_Deep;

    // Use this for initialization
    void Start()
    {
        // Setting up the layermask that will ignore water (used for finding ground/walls without finding water by mistake)
        LayerMask WaterMask_Surface = 1 << LayerMask.NameToLayer("Water");
        LayerMask WaterMask_Deep = 1 << LayerMask.NameToLayer("Water_Deep");
        WaterMask = WaterMask_Surface | WaterMask_Deep;
        WaterMask = ~WaterMask;
        rigidBody = GetComponent<Rigidbody>();
        MyPos = GetComponent<DeltaPosTracker>();
        //groundFinder = transform.Find("GroundFinder").GetComponent<GroundFinder_3DPlatformer>();
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
    }

    private void FixedUpdate()
    {
        //print(MovementVector);
        if (ControlsEnabled && !Jumping)
        {
            // Adding movement for following moving platforms
            if (PlatformDelta != null)
            {
                transform.RotateAround(PlatformDelta.transform.position, Vector3.up, PlatformDelta.GetDeltaRotation());// * Vector3.Distance(transform.position, PlatformDelta.transform.position));
                rigidBody.MovePosition(rigidBody.position + PlatformDelta.GetDeltaPos());

                Vector3 _AddMovement = Quaternion.AngleAxis(PlatformDelta.GetDeltaRotation(), Vector3.up) * transform.position;

                Vector3 _AddMovement_Walls = (Vector3)CapsulecastForWalls(MyPos.GetDeltaPos().normalized, 0.05f)[0];
                if (Vector3.Angle(_AddMovement_Walls, Vector3.up) > MaxSlope)
                {
                    _AddMovement_Walls = new Vector3(Mathf.Abs(_AddMovement_Walls.x), 0, Mathf.Abs(_AddMovement_Walls.z)) * -1;
                    _AddMovement_Walls *= Vector3.Distance(transform.position, transform.position + MyPos.GetDeltaPos());
                    MovementVector += _AddMovement_Walls;
                }
            }
            Movement();
        }
        // Clamping movement speed
        Vector2 _HorizSpeed = new Vector2(rigidBody.velocity.x, rigidBody.velocity.z);
        switch (Mode)
        {
            case MoveMode.Grounded:
                bool _Grounded = (bool)SpherecastForGround(rigidBody.position)[0];
                if (!_Grounded && !GrabbingLedge)
                {
                    // Fake gravity
                    rigidBody.AddForce(Vector3.Scale(Vector3.up, Physics.gravity * 5), ForceMode.Acceleration);

                    // Starting fall damage timer
                    if (rigidBody.velocity.y < 0 && Falling == null && !FallDamage)
                    {
                        Falling = StartCoroutine(FallDamageTimer());
                    }

                    // Clamp aerial movement speed
                    _HorizSpeed = Vector2.ClampMagnitude(_HorizSpeed, MoveSpeed);
                    if (FallDamage)
                    {
                        rigidBody.velocity = new Vector3(_HorizSpeed.x, Mathf.Clamp(rigidBody.velocity.y, -40.0f, 30.0f), _HorizSpeed.y);
                    }
                    else
                    {
                        rigidBody.velocity = new Vector3(_HorizSpeed.x, Mathf.Clamp(rigidBody.velocity.y, -30.0f, 30.0f), _HorizSpeed.y);
                    }
                }
                // Cancel fall damage when grounded
                else if (_Grounded)
                {
                    if (FallDamage)
                    {
                        // TODO: Make the player take fall damage
                        print("Took fall damage!");
                    }
                    ResetJump();
                }
                break;
            case MoveMode.Flying:
                if ((bool)SpherecastForGround(rigidBody.position)[0])
                {
                    ChangeMode_Grounded();
                }
                else
                {
                    // Fake gravity
                    rigidBody.AddForce(Vector3.Scale(Vector3.up, Physics.gravity), ForceMode.Acceleration);

                    // Clamp aerial movement speed
                    _HorizSpeed = Vector2.ClampMagnitude(_HorizSpeed, MoveSpeed);
                    // Rapid descent while holding the Crouch button
                    if (Crouching)
                    {
                        rigidBody.velocity = new Vector3(_HorizSpeed.x, Mathf.Clamp(rigidBody.velocity.y, -30.0f, 30.0f), _HorizSpeed.y);

                        // Starting fall damage timer
                        if (Falling == null)
                        {
                            Falling = StartCoroutine(FallDamageTimer());
                        }
                    }
                    else
                    {
                        rigidBody.velocity = new Vector3(_HorizSpeed.x, Mathf.Clamp(rigidBody.velocity.y, -5.0f, 30.0f), _HorizSpeed.y);
                        // Cancel fall damage if the player starts using the jetpack again before hitting the ground
                        StopFalling();
                    }
                }
                break;
            case MoveMode.Swimming_Surface:
                _HorizSpeed = Vector2.ClampMagnitude(_HorizSpeed, MoveSpeed * 0.5f);
                rigidBody.velocity = new Vector3(_HorizSpeed.x, Mathf.Clamp(rigidBody.velocity.y, -MoveSpeed, MoveSpeed), _HorizSpeed.y);
                if (MovementVector.y >= 0)
                {
                    rigidBody.MovePosition(Vector3.Lerp(rigidBody.position, new Vector3(rigidBody.position.x, GetWaterSurface(), rigidBody.position.z), Time.fixedDeltaTime * 10));
                }
                break;
            case MoveMode.Swimming_Deep:
                _HorizSpeed = Vector2.ClampMagnitude(_HorizSpeed, MoveSpeed * 0.5f);
                rigidBody.velocity = new Vector3(_HorizSpeed.x, Mathf.Clamp(rigidBody.velocity.y, -MoveSpeed * 0.5f, MoveSpeed * 0.5f), _HorizSpeed.y);
                break;
        }
        
        // Changing between swimming modes
        if (TouchingWater_Surface && TouchingWater_Deep && !FallDamage)
        {
            if(rigidBody.velocity.y < 0 && Mode == MoveMode.Grounded || rigidBody.velocity.y < 0 && Mode == MoveMode.Flying || Mode == MoveMode.Swimming_Deep)
            {
                //StartCoroutine(FreezePosition(rigidBody.position));
                ChangeMode_Swimming_Surface();
            }
        }
        else if (!TouchingWater_Surface && TouchingWater_Deep)
        {
            ChangeMode_Swimming_Deep();
        }
        else if (TouchingWater_Surface && !TouchingWater_Deep && Mode != MoveMode.Grounded)
        {
            ChangeMode_Grounded();
        }

        // Dismounting a ladder if touching the ground
        if(Mode == MoveMode.Climbing_Ladder || Mode == MoveMode.Climbing_Pole)
        {
            bool _Grounded = (bool)SpherecastForGround(rigidBody.position)[0];
            if (_Grounded)
            {
                ChangeMode_Grounded();
                StartCoroutine(MaintainGroundedState());
            }
        }
    }

    private void GetInput()
    {
        MovementVector = new Vector3();
        if (Input.GetButton("Move Fwd"))
        {
            if(Mode != MoveMode.Climbing_Ladder && Mode != MoveMode.Climbing_Pole)
            {
                MovementVector += new Vector3(0, 0, 1);
                //MovementVector += ForwardRef.forward;
            }
            else
            {
                MovementVector += new Vector3(0, 1, 0);
            }
        }
        if (Input.GetButton("Move Back"))
        {
            if (Mode != MoveMode.Climbing_Ladder && Mode != MoveMode.Climbing_Pole)
            {
                MovementVector += new Vector3(0, 0, -1);
                //MovementVector += -ForwardRef.forward;
            }
            else
            {
                MovementVector += new Vector3(0, -1, 0);
            }
        }
        if (Input.GetButton("Move Left"))
        {
            MovementVector += new Vector3(-1, 0, 0);
            //MovementVector += -ForwardRef.right;
        }
        if (Input.GetButton("Move Right"))
        {
            MovementVector += new Vector3(1, 0, 0);
            //MovementVector += ForwardRef.right;
        }

        // Get joystick input
        Vector3 _JoystickMovement = new Vector3(Input.GetAxis("Move Horiz"), 0, Input.GetAxis("Move Vert"));
        if(Vector3.Magnitude(_JoystickMovement) > ControllerDeadZone)
        {
            MovementVector += _JoystickMovement.normalized;
        }
        
        // Don't climb if at the top of a ladder
        if (Mode == MoveMode.Climbing_Ladder || Mode == MoveMode.Climbing_Pole)
        {
            if (!RaycastForLadderTop())
            {
                //print("Reached top of ladder");
                MovementVector += new Vector3(0, -1, 0);
            }
        }

        // Set raw movement vector
        MovementVector_Raw = MovementVector;

        // Rotate movement vector to match camera forward angle
        MovementVector = Quaternion.AngleAxis(ForwardRef.eulerAngles.y, Vector3.up) * MovementVector;

        // Make player face their movement direction when moving while grounded
        if (Mode == MoveMode.Grounded || Mode == MoveMode.Swimming_Surface || Mode == MoveMode.Swimming_Deep)
        {
            if (MovementVector != Vector3.zero && !GrabbingLedge && ControlsEnabled)
            {
                if(AttackActive_Roll != null)
                {
                    transform.LookAt(rigidBody.position + Vector3.Scale(SavedMovement, new Vector3(1, 0, 1)).normalized);
                }
                else if (Vector3.Angle(transform.forward, MovementVector) > 175 && Vector3.Angle(transform.forward, MovementVector) < 181)
                {
                    transform.LookAt(Vector3.Lerp(rigidBody.position + transform.forward, rigidBody.position + transform.right, Time.fixedDeltaTime * 10));
                }
                transform.LookAt(Vector3.Lerp(rigidBody.position + transform.forward, rigidBody.position + Vector3.Scale(MovementVector, new Vector3(1, 0, 1)).normalized, Time.fixedDeltaTime * 10));
            }
        }

        // Rotate player based on camera movement while flying
        else if(Mode == MoveMode.Flying)
        {
            if(Mathf.Abs(Input.GetAxis("Cam Horiz")) > ControllerDeadZone || Mathf.Abs(Input.GetAxis("Cam Mouse Horiz")) > ControllerDeadZone)
            {
                float _Rotation = Input.GetAxis("Cam Horiz");
                if (Mathf.Abs(Input.GetAxis("Cam Mouse Horiz")) > ControllerDeadZone)
                {
                    _Rotation = Input.GetAxis("Cam Mouse Horiz");
                    _Rotation *= FlyingRotationSensitivity_Joystick;
                }
                else
                {
                    _Rotation *= FlyingRotationSensitivity_Mouse;
                }
                rigidBody.MoveRotation(Quaternion.AngleAxis(transform.eulerAngles.y + _Rotation, Vector3.up));
            }
            // Tilt camera based on movement
            ForwardRef.GetComponent<CameraController_ThirdPerson>().SetTargetRotationModifier(Vector3.Lerp(ForwardRef.GetComponent<CameraController_ThirdPerson>().GetTargetRotationModifier(), MovementVector_WorldAxes * 5, Time.fixedDeltaTime * 2));
            //print(ForwardRef.GetComponent<CameraController_ThirdPerson>().GetTargetRotationModifier());
        }

        // Normalize movement vector
        MovementVector.Normalize();
        
        // Scale movement to match controller input
        if (Vector3.Magnitude(_JoystickMovement) > ControllerDeadZone)
        {
            print("Joystick movement is " + _JoystickMovement);
            _JoystickMovement = Quaternion.AngleAxis(ForwardRef.eulerAngles.y, Vector3.up) * _JoystickMovement;
            print("Rotated joystick movement is " + _JoystickMovement);
            //Vector3 _JoystickMovement_Rotated = Quaternion.AngleAxis(ForwardRef.rotation.y, Vector3.up) * _JoystickMovement;
            print("Unscaled movement is " + MovementVector);
            MovementVector = Vector3.Scale(MovementVector, new Vector3(Mathf.Abs(_JoystickMovement.x), 1, Mathf.Abs(_JoystickMovement.z)));
            print("Scaled movement is " + MovementVector);
        }

        //Factor in wall normals for movement
        WallNormal = (Vector3)CapsulecastForWalls(MovementVector, 0.05f)[0];
        //print("Wall angle is " + Vector3.Angle(WallNormal, Vector3.up));
        //print("Wall normal is " + WallNormal);
        if(Vector3.Angle(WallNormal, Vector3.up) > MaxSlope)
        {
            WallNormal = new Vector3(Mathf.Abs(WallNormal.x), 0, Mathf.Abs(WallNormal.z)) * -1;
            MovementVector += Vector3.Scale(MovementVector, WallNormal.normalized);
        }

        // Factor in ground normal for movement if swimming in deep water (doesn't seem to work?)
        if(Mode == MoveMode.Swimming_Deep)
        { 
            // Factoring in ground normal for movement if the player is at the bottom of a water body
            bool _GroundBeneath = (bool)SpherecastForGround(rigidBody.position)[0] && Vector3.Angle(((Vector3)SpherecastForGround(rigidBody.position)[1]), Vector3.up) < MaxSlope;
            if (_GroundBeneath)
            {
                GroundNormal = (Vector3)SpherecastForGround(rigidBody.position)[1];
                GroundNormal = new Vector3(Mathf.Abs(GroundNormal.x), Mathf.Abs(GroundNormal.y), Mathf.Abs(GroundNormal.z)) * -1;
                //print(GroundNormal);

                MovementVector += Vector3.Scale(MovementVector, GroundNormal.normalized);
            }
        }

        // Get the non-rotated movement vector
        //MovementVector_WorldAxes = Quaternion.AngleAxis(-transform.eulerAngles.y + 90, Vector3.up) * MovementVector;
        MovementVector_WorldAxes = Quaternion.Euler(-transform.eulerAngles + new Vector3(0, 90, 0)) * MovementVector;

        //print("Movement vector is " + MovementVector);

        // Crouching
        Crouching = Input.GetButton("Crouch");

        // Diving while swimming
        if(Crouching && Mode == MoveMode.Swimming_Deep || Crouching && Mode == MoveMode.Swimming_Surface)
        {
            MovementVector += -Vector3.up;
        }

        // Rising while underwater
        // NOTE: This is separate from the below jumping functions because it relies on holding the Jump button whereas the below only checks for the frame when it is first pressed.
        if (Input.GetButton("Jump") && Mode == MoveMode.Swimming_Deep)
        {
            MovementVector += Vector3.up;
        }

        // Overriding movement when rolling
        if (AttackActive_Roll != null)
        {
            if(MovementVector != Vector3.zero && Vector3.Angle(MovementVector, SavedMovement) > 10)
            {
                SavedMovement = Vector3.RotateTowards(SavedMovement, MovementVector, Time.fixedDeltaTime * 3, 0.0f);
            }
            MovementVector = SavedMovement;
        }

        // Jumping
        if (Input.GetButtonDown("Jump"))
        {
            //print("Spacebar Pressed");
            //print("Can Jump = " + !Jumping + ", Can Double Jump = " + CanDoubleJump);
            switch (Mode)
            {
                case MoveMode.Grounded:
                    if (Crouching)
                    {
                        Jump();
                        ChangeMode_Flying();
                    }
                    else if (!Jumping && Grounded || !Jumping && GrabbingLedge)
                    {
                        Jump();
                        //print("First Jump - " + CanDoubleJump);
                    }
                    else if (!Jumping && !Grounded)
                    {
                        if (CanDoubleJump)
                        {
                            CanDoubleJump = false;
                            Jump();
                            //print("Double-Jump");
                        }
                        // Gliding
                        else if(Gliding == null && !GlideOver)
                        {
                            Gliding = StartCoroutine(Glide());
                        }
                    }
                    break;
                case MoveMode.Flying:
                    if (!Jumping && !Crouching)
                    {
                        Jump_Flying();
                    }
                    break;
                case MoveMode.Swimming_Surface:
                case MoveMode.Climbing_Ladder:
                case MoveMode.Climbing_Pole:
                    Jump();
                    ChangeMode_Grounded();
                    break;
                case MoveMode.Swimming_Deep:
                    break;
            }
        }

        // Attacking
        if (Input.GetButtonDown("Attack"))
        {
            switch (Mode)
            {
                case MoveMode.Grounded:
                    // Roll attack while moving
                    if(MovementVector != Vector3.zero)
                    {
                        AttackActive_Roll = StartCoroutine(Attack_Roll());
                    }
                    // Punch attack while standing
                    else
                    {
                        AttackActive_Punch = StartCoroutine(Attack_Punch());
                    }
                    break;
                case MoveMode.Flying:
                    break;
            }
        }
    }

    void Movement()
    {
        switch (Mode)
        {
            case MoveMode.Grounded:
                //print("Moving" + MovementVector);
                GroundNormal = Vector3.up;
                // Check for ground beneath the player (to see if they are grounded)
                bool _GroundBeneath = (bool)SpherecastForGround(rigidBody.position)[0] && Vector3.Angle(((Vector3)SpherecastForGround(rigidBody.position)[1]), Vector3.up) < MaxSlope;

                //print("Max slope is " + MaxSlope);
                //print("Ground angle is " + Vector3.Angle(((Vector3)SpherecastForGround(rigidBody.position)[1]), transform.up));

                // Grounded
                if (_GroundBeneath)
                {
                    // Reset grounded and jumping flags to allow for grounded movement and jumping
                    Grounded = true;
                    CanDoubleJump = true;
                    if(LateJumpTimerRunning != null)
                    {
                        StopLateJumpTimer();
                    }
                    GrabbingLedge = false;

                    // Check for ground ahead of the player (to get the ground normal)
                    bool _GroundAhead = (bool)RaycastForGround(rigidBody.position + (MovementVector * 0.25f))[0];
                    if (_GroundAhead)
                    {
                        GroundNormal = (Vector3)RaycastForGround(rigidBody.position + (MovementVector * 0.25f))[1] + (Vector3)SpherecastForGround(rigidBody.position)[1];
                        GroundNormal *= 0.5f;
                        //print("Ground Normal Y is " + _GroundNormal.y);
                        GroundNormal.Normalize();
                    }
                    else
                    {
                        GroundNormal = (Vector3)SpherecastForGround(rigidBody.position)[1];
                        //print(_GroundNormal);
                    }

                    MovementVector = Vector3.Cross(Vector3.Cross(MovementVector, GroundNormal), GroundNormal);
                    MovementVector *= -1;

                    // Apply movement
                    rigidBody.velocity = MovementVector * MoveSpeed;
                }
                // Airborne
                else if (!GrabbingLedge)
                {
                    //print("Airborne");
                    if (LateJumpTimerRunning == null && Grounded && !Jumping)
                    {
                        LateJumpTimerRunning = StartCoroutine(LateJumpTimer());
                    }
                    // Aerial movement
                    rigidBody.AddForce(MovementVector * MoveSpeed * 6, ForceMode.Acceleration);
                    // Slow down aerial movement if no input is given
                    if (MovementVector == new Vector3())
                    {
                        rigidBody.velocity = Vector3.Scale(rigidBody.velocity, new Vector3(AerialDrag, 1, AerialDrag));
                    }

                    // Search for a grabbable ledge if falling
                    /*if (rigidBody.velocity.y < 0 && !Jumping)
                    {
                        ArrayList _LedgeCheck = SpherecastForGrabLedge(rigidBody.position + (transform.forward * 1.0f) + transform.up);
                        bool _FoundLedge = (bool)_LedgeCheck[0];
                        Vector3 _LedgePoint = (Vector3)_LedgeCheck[1];
                        if (_FoundLedge)
                        {
                            print("Found ledge at " + _LedgePoint + " - Distance is " + (float)_LedgeCheck[2]);
                            GrabLedge(_LedgePoint);
                        }
                    }*/
                }
                break;
            case MoveMode.Flying:
                // Aerial movement
                rigidBody.AddForce(MovementVector * MoveSpeed * 3, ForceMode.Acceleration);
                // Slow down aerial movement if no input is given
                if (MovementVector == new Vector3())
                {
                    rigidBody.velocity = Vector3.Scale(rigidBody.velocity, new Vector3(AerialDrag, 1, AerialDrag));
                }
                if (Crouching)
                {
                    rigidBody.AddForce(Physics.gravity * 3, ForceMode.Acceleration);
                }
                break;
            case MoveMode.Climbing_Ladder:
                rigidBody.velocity = Vector3.zero;
                rigidBody.MovePosition(Vector3.Lerp(rigidBody.position, new Vector3(LadderPosition.x, rigidBody.position.y + (MovementVector.y * ClimbSpeed), LadderPosition.z), Time.fixedDeltaTime * 10));
                break;
            case MoveMode.Climbing_Pole:
                rigidBody.velocity = Vector3.zero;
                PoleOffset = Quaternion.AngleAxis(-MovementVector_Raw.x * (1 / PoleRadius), Vector3.up) * PoleOffset;
                //rigidBody.MovePosition(Vector3.Lerp(rigidBody.position, new Vector3(rigidBody.position.x, rigidBody.position.y + (MovementVector_Raw.y * ClimbSpeed), rigidBody.position.z), Time.fixedDeltaTime * 10));
                rigidBody.MovePosition(new Vector3(LadderPosition.x + PoleOffset.x, Mathf.Lerp(rigidBody.position.y, rigidBody.position.y + (MovementVector_Raw.y * ClimbSpeed), Time.fixedDeltaTime * 10), LadderPosition.z + PoleOffset.z));
                //transform.Rotate(Vector3.up, MovementVector.x * PoleRotationSpeed * Time.fixedDeltaTime);
                transform.LookAt(new Vector3(LadderPosition.x, transform.position.y, LadderPosition.z));
                //print("Velocity is " + rigidBody.velocity);
                break;
            case MoveMode.Swimming_Surface:
                rigidBody.AddForce(MovementVector * MoveSpeed * 3, ForceMode.Acceleration);
                if (MovementVector == new Vector3())
                {
                    rigidBody.velocity = Vector3.Scale(rigidBody.velocity, new Vector3(AerialDrag, 1, AerialDrag));
                }

                // Cancel swimming if not in water
                if (!TouchingWater_Deep && !TouchingWater_Surface)
                {
                    ChangeMode_Grounded();
                }
                break;
            case MoveMode.Swimming_Deep:
                rigidBody.AddForce(MovementVector * MoveSpeed * 3, ForceMode.Acceleration);

                if (MovementVector.x == 0 && MovementVector.z == 0)
                {
                    rigidBody.velocity = Vector3.Scale(rigidBody.velocity, new Vector3(WaterDrag, 1, WaterDrag));
                }
                if (MovementVector.y == 0)
                {
                    rigidBody.velocity = Vector3.Scale(rigidBody.velocity, new Vector3(1, WaterDrag, 1));
                }

                // Cancel swimming if not in water
                if(!TouchingWater_Deep && !TouchingWater_Surface)
                {
                    ChangeMode_Grounded();
                }
                break;
        }
    }

    void Jump()
    {
        rigidBody.velocity = Vector3.Scale(rigidBody.velocity, new Vector3(1, 0, 1));
        rigidBody.AddForce(Vector3.up * JumpPower, ForceMode.Impulse);
        StartCoroutine(JumpActivateTimer());
        GrabbingLedge = false;
        if(AttackActive_Roll != null)
        {
            StopCoroutine(AttackActive_Roll);
            AttackActive_Roll = null;
        }
        StopFalling();
    }

    void Jump_Flying()
    {
        rigidBody.velocity = Vector3.Scale(rigidBody.velocity, new Vector3(1, 0, 1));
        rigidBody.AddForce(Vector3.up * JumpPower_Flying, ForceMode.Impulse);
        StartCoroutine(JumpActivateTimer_Flying());
    }

    // [UNUSED: Ledge-grabbing is buggy and inconsistent and is therefore disabled for now]
    void GrabLedge(Vector3 _LedgePoint)
    {
        GrabbingLedge = true;
        Vector3 _WallNormal = (Vector3)CapsulecastForWalls(transform.forward, 1.5f)[0];
        Vector3 _WallPoint = (Vector3)CapsulecastForWalls(transform.forward, 1.5f)[1];
        StartCoroutine(FreezePosition(new Vector3(_WallPoint.x, _LedgePoint.y - 0.5f, _WallPoint.z) + (_WallNormal * 0.5f)));
        transform.LookAt(transform.position - _WallNormal);
        Jumping = false;
        CanDoubleJump = true;
    }
    
    float GetWaterSurface()
    {
        //print(rigidBody.velocity.y);
        float _WaterDepth = RaycastForWaterDepth();
        //print("Water height is " + _WaterDepth);
        return _WaterDepth;
    }

    public void ChangeMode_Grounded()
    {
        if(Mode == MoveMode.Climbing_Ladder || Mode == MoveMode.Climbing_Pole)
        {
            StartCoroutine(WaitToReGrabLadder());
        }
        Mode = MoveMode.Grounded;
        ForwardRef.GetComponent<CameraController_ThirdPerson>().ChangeMode_Normal();
        ForwardRef.GetComponent<ObjectFollower>().FollowOffset = new Vector3(0, 3, 0);
    }

    public void ChangeMode_Flying()
    {
        Mode = MoveMode.Flying;
        ForwardRef.GetComponent<CameraController_ThirdPerson>().ChangeMode_Flying();
        ForwardRef.GetComponent<ObjectFollower>().FollowOffset = Vector3.zero;
    }

    public void ChangeMode_Climbing_Ladder(Vector3 _LookDirection, Vector3 _Position)
    {
        if(Mode == MoveMode.Grounded || Mode == MoveMode.Flying)
        {
            ResetJump();

            transform.LookAt(transform.position + _LookDirection);
            LadderPosition = _Position;
            Mode = MoveMode.Climbing_Ladder;
        }
    }

    public void ChangeMode_Climbing_Pole(Vector3 _LookDirection, Vector3 _Position, float _Radius)
    {
        // Cannot switch to climbing mode except while walking or flying
        if (Mode == MoveMode.Grounded || Mode == MoveMode.Flying)
        {
            ResetJump();

            transform.LookAt(transform.position + new Vector3(_LookDirection.x, 0, _LookDirection.z));
            LadderPosition = _Position;
            PoleRadius = _Radius;
            PoleOffset = LadderPosition - transform.position;
            PoleOffset = Quaternion.AngleAxis(Mathf.Abs(Vector3.Angle(transform.position, new Vector3(LadderPosition.x, transform.position.y, LadderPosition.z))), Vector3.up) * Vector3.forward;
            PoleOffset = PoleOffset.normalized;// * -PoleRadius;
            print("PoleOffset is " + PoleOffset);
            Mode = MoveMode.Climbing_Pole;
        }
    }

    public void ChangeMode_Swimming_Surface()
    {
        if (Mode == MoveMode.Swimming_Deep)
        {
            rigidBody.velocity = Vector3.Scale(rigidBody.velocity, new Vector3(1, 0.5f, 1));
            rigidBody.angularVelocity = Vector3.Scale(rigidBody.angularVelocity, new Vector3(1, 0.5f, 1));
        }
        else
        {
            rigidBody.velocity = Vector3.Scale(rigidBody.velocity, new Vector3(1, 0, 1));
            rigidBody.angularVelocity = Vector3.Scale(rigidBody.angularVelocity, new Vector3(1, 0, 1));
        }
        ResetJump();
        // Clear PlatformDelta
        if (PlatformDelta != null)
        {
            PlatformDelta = null;
        }
        Mode = MoveMode.Swimming_Surface;
    }

    public void ChangeMode_Swimming_Deep()
    {
        ResetJump();
        // Clear PlatformDelta
        if (PlatformDelta != null)
        {
            PlatformDelta = null;
        }
        Mode = MoveMode.Swimming_Deep;
    }

    // Tell other scripts if the player is currently in a situation where they can mount a ladder or pole
    public bool CanMountLadder()
    {
        if (OverrideLadderGrab)
        {
            return false;
        }
        return (Mode == MoveMode.Grounded && !Grounded && rigidBody.velocity.y < 1.0f && !FallDamage || Mode == MoveMode.Flying && !FallDamage);
    }

    // Dismount a ladder
    /*public void DismountLadder()
    {
        ChangeMode_Grounded();
        if (!Grounded && rigidBody.velocity.y > 0)
        {
            Jump();
        }
    }*/


    private void OnCollisionEnter(Collision collision)
    {
        GameObject _otherObject = collision.gameObject;

        if(_otherObject.tag == "Untagged" || _otherObject.tag == "World")
        {
            foreach(ContactPoint _Point in collision.contacts)
            {
                if(_Point.normal.y <= MaxSlope)
                {
                    GroundNormal = _Point.normal;
                }
                else
                {
                    WallNormal = _Point.normal;
                }
            }
        }
    }

    void StopLateJumpTimer()
    {
        if(LateJumpTimerRunning != null)
        {
            StopCoroutine(LateJumpTimerRunning);
            LateJumpTimerRunning = null;
        }
    }

    void StopFalling()
    {
        if(Falling != null)
        {
            StopCoroutine(Falling);
            Falling = null;
        }
        if (FallDamage)
        {
            FallDamage = false;
        }
    }

    void EndGlide()
    {
        if(Gliding != null)
        {
            StopCoroutine(Gliding);
            Gliding = null;
        }
        GlideOver = true;
        InGlide = false;
    }

    void ResetJump()
    {
        StopFalling();
        EndGlide();
        GlideOver = false;
        CanDoubleJump = true;
    }

    // --------------------------------------------------

    // Raycast for ground in front of the player (used for getting an averaged ground normal if the player is near two planes with different slopes)
    ArrayList RaycastForGround(Vector3 _StartPos)
    {
        ArrayList _array = new ArrayList();
        Vector3 _GroundNormal = Vector3.up;
        float _RayLength = 1.5f;
        Ray _GroundRay = new Ray(_StartPos, Vector3.down);
        //Debug.DrawRay(_StartPos, Vector3.down * _RayLength, Color.red);
        RaycastHit _HitInfo;
        bool _HitGround = Physics.Raycast(_GroundRay.origin, _GroundRay.direction, out _HitInfo, _RayLength, WaterMask, QueryTriggerInteraction.Ignore);
        if (_HitGround)
        {
            //print("Hitting " + _HitInfo.collider + " with RaycastForGround()");
            _GroundNormal = _HitInfo.normal;
            //GroundPoint = _HitInfo.point;
        }
        _array.Add(_HitGround);
        _array.Add(_GroundNormal);

        return _array;
    }

    // Spherecast to find ground directly beneath the player
    ArrayList SpherecastForGround(Vector3 _StartPos)
    {
        ArrayList _array = new ArrayList();
        Vector3 _GroundNormal = Vector3.up;
        Ray _GroundRay = new Ray(_StartPos, Vector3.down);
        RaycastHit _HitInfo;
        bool _HitGround = Physics.SphereCast(_GroundRay.origin, 0.3f, _GroundRay.direction, out _HitInfo, 1.0f, WaterMask, QueryTriggerInteraction.Ignore);
        if (_HitGround)
        {
            //print("Hitting " + _HitInfo.collider + " with SpherecastForGround()");
            _GroundNormal = _HitInfo.normal;
            // Set PlatformDelta to the object's DeltaPosTracker if it has one
            DeltaPosTracker _Tracker = _HitInfo.transform.GetComponent<DeltaPosTracker>();
            //print(_Tracker);
            if (_Tracker != null && PlatformDelta != _Tracker)
            {
                PlatformDelta = _Tracker;
                //print("PlatformDelta is now " + PlatformDelta);
            }
            // Otherwise, clear PlatformDelta
            else if (_Tracker == null && PlatformDelta != null)
            {
                PlatformDelta = null;
            }
            //Debug.DrawRay(_HitInfo.point, Vector3.up, Color.red);
            //GroundPoint = _HitInfo.point;
        }
        // Clear PlatformDelta if no ground is found
        else if (!_HitGround && PlatformDelta != null)
        {
            PlatformDelta = null;
        }
        _array.Add(_HitGround);
        _array.Add(_GroundNormal);

        return _array;
    }

    // Capsule cast to find walls and factor them in for movement
    ArrayList CapsulecastForWalls(Vector3 _Direction, float _MaxDistance)
    {
        ArrayList _array = new ArrayList();
        Vector3 _WallNormal = new Vector3();
        Vector3 _WallPoint = new Vector3();
        Ray _WallRay = new Ray(rigidBody.position, _Direction);
        RaycastHit _HitInfo;
        float _BehindDistance = 0.75f;
        Vector3 _JustBehind = new Vector3(-transform.forward.x, 0, -transform.forward.z) * _BehindDistance;
        //Debug.DrawRay(_WallRay.origin + _JustBehind, -transform.forward, Color.red, 2.0f);
        //Debug.DrawLine(_WallRay.origin + new Vector3(_JustBehind.x, 0, _JustBehind.z), rigidBody.position + transform.forward * (_MaxDistance + _BehindDistance), Color.blue, 2.0f);
        bool _HitWall = Physics.CapsuleCast(_WallRay.origin + new Vector3(_JustBehind.x, 0.5f, _JustBehind.z), _WallRay.origin + new Vector3(_JustBehind.x, -0.5f, _JustBehind.z), 0.55f, _WallRay.direction, out _HitInfo, _MaxDistance + _BehindDistance, WaterMask, QueryTriggerInteraction.Ignore);
        if (_HitWall && Vector3.Angle(_HitInfo.normal, Vector3.up) > MaxSlope)
        {
            //print("Hitting " + _HitInfo.collider + " with CapsulecastForWalls()");
            //print("Hit Wall - " + _HitInfo.collider.name);
            //Debug.DrawRay(_HitInfo.point, _HitInfo.normal, Color.green, 2.0f);
            _WallNormal = _HitInfo.normal;
            _WallPoint = _HitInfo.point;
        }

        //print("Adding wall normal to array");
        _array.Add(_WallNormal);
        //print("Adding wall point to array");
        _array.Add(_WallPoint);
        //print("Returning array");
        return _array;
    }

    // Searching for the top of a ladder to know when to stop climbing
    bool RaycastForLadderTop()
    {
        Ray _LadderRay = new Ray(transform.position + transform.up - (transform.forward * 0.5f), transform.forward);
        RaycastHit _HitInfo;
        LayerMask _LadderMask = 1 << LayerMask.NameToLayer("Ladder");
        LayerMask _PlayerMask = 1 << LayerMask.NameToLayer("Player");
        _LadderMask = _LadderMask | ~_PlayerMask;
        //_LadderMask = ~_LadderMask;
        bool _HitLadder = Physics.Raycast(_LadderRay.origin, _LadderRay.direction, out _HitInfo, 1 + PoleRadius, _LadderMask, QueryTriggerInteraction.Collide);
        Debug.DrawRay(_LadderRay.origin, _LadderRay.direction * (1 + PoleRadius), Color.red, 2.0f);
        print("Object hit is " + _HitInfo.collider);
        return _HitLadder;
    }

    // [UNUSED: Raycast to find a grabbable ledge (doesn't work consistently)]
    ArrayList SpherecastForGrabLedge(Vector3 _StartPos)
    {
        ArrayList _array = new ArrayList();
        Vector3 _LedgePoint = new Vector3();
        float _RayLength = 0.25f;
        Ray _LedgeRay = new Ray(_StartPos, Vector3.down);
        RaycastHit _HitInfo;
        bool _HitLedge = Physics.SphereCast(_LedgeRay.origin, _RayLength, _LedgeRay.direction, out _HitInfo, 0.5f);
        if (_HitLedge && Vector3.Angle(_HitInfo.normal, Vector3.up) < MaxSlope && !_HitInfo.collider.isTrigger)
        {
            //print("Hitting a ledge");
            _LedgePoint = _HitInfo.point;
            //print(_LedgePoint);
            //print("Got _LedgePoint - " + _LedgePoint);
            //Debug.DrawRay(_LedgePoint, Vector3.up * _RayLength, Color.red, Mathf.Infinity);
        }
        _array.Add(_HitLedge);
        _array.Add(_LedgePoint);
        _array.Add(_HitInfo.distance);
        /*if(_HitLedge)
            print("_LedgePoint is " + _array[1]);*/
        return _array;
    }
    
    float RaycastForWaterDepth()
    {
        float _WaterDepth = transform.position.y;
        Ray _WaterRay = new Ray(transform.position + (Vector3.up * 2), -Vector3.up);
        RaycastHit _HitInfo;
        LayerMask _PlayerLayer = 1 << LayerMask.NameToLayer("Player");
        LayerMask _WaterCheck = ~_PlayerLayer | WaterMask_Deep;
        bool _HitWater = Physics.Raycast(_WaterRay.origin, _WaterRay.direction, out _HitInfo, 3.0f, _WaterCheck);
        if (_HitWater)
        {
            //print("Hit " + _HitInfo.collider + " at Y height " + _HitInfo.point.y);
            _WaterDepth = _HitInfo.point.y;
        }
        //Debug.DrawRay(_HitInfo.point, Vector3.up, Color.blue, 2.0f);
        return _WaterDepth;
    }

    // --------------------------------------------------

    IEnumerator MaintainGroundedState()
    {
        Grounded = true;
        yield return new WaitForEndOfFrame();
    }

    IEnumerator LateJumpTimer()
    {
        yield return new WaitForSeconds(LateJumpDelay);
        Grounded = false;
    }

    IEnumerator JumpActivateTimer()
    {
        Jumping = true;
        Grounded = false;
        yield return new WaitForFixedUpdate();
        Jumping = false;
    }

    IEnumerator JumpActivateTimer_Flying()
    {
        Jumping = true;
        float i = 0;
        while (i > 0.75f)
        {
            i += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        Jumping = false;
    }

    IEnumerator WaitToReGrabLadder()
    {
        OverrideLadderGrab = true;
        yield return new WaitForSeconds(0.33f);
        OverrideLadderGrab = false;
    }

    IEnumerator FreezePosition(Vector3 _NewPosition)
    {
        print("Freeze Position");
        rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
        rigidBody.MovePosition(_NewPosition);
        rigidBody.velocity = Vector3.zero;
        rigidBody.angularVelocity = Vector3.zero;
        yield return new WaitForSeconds(0.1f);
        //rigidBody.velocity = new Vector3();
        rigidBody.constraints = RigidbodyConstraints.FreezeAll;
        yield return new WaitForSeconds(0.1f);
        rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
    }

    IEnumerator Attack_Punch()
    {
        yield return new WaitForFixedUpdate();
    }

    IEnumerator Attack_Roll()
    {
        SavedMovement = Vector3.Scale(MovementVector, new Vector3(1, 0, 1)).normalized;
        float i = 0;
        while(i < 0.4f)
        {
            i += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        // Continue rolling as long as the Attack button is held (comment this block out to disable this if it gets weird/annoying/impractical)
        while (Input.GetButton("Attack"))
        {
            i = 0;
            while (i < 0.4f)
            {
                i += Time.deltaTime;
                yield return new WaitForFixedUpdate();
            }
            yield return new WaitForFixedUpdate();
        }
        SavedMovement = Vector3.zero;
        AttackActive_Roll = null;
    }

    IEnumerator FallDamageTimer()
    {
        float _FallTime = 0;
        while (_FallTime < FallDamageTime)
        {
            if (!InGlide)
            {
                _FallTime += Time.deltaTime;
            }
            yield return new WaitForFixedUpdate();
        }
        print("WARNING: Fall damage imminent!");
        FallDamage = true;
    }

    IEnumerator Glide()
    {
        float _GlideTimer = 0;
        while (_GlideTimer < MaxGlideTime)
        {
            if (Input.GetButton("Jump"))
            {
                rigidBody.velocity = new Vector3(rigidBody.velocity.x, -GlideDescentSpeed, rigidBody.velocity.z);
                _GlideTimer += Time.deltaTime;
                StopFalling();
            }
            InGlide = Input.GetButton("Jump");
            yield return new WaitForFixedUpdate();
        }
        EndGlide();
    }

    // [NOTE: Currently unused as it doesn't work right]
    IEnumerator Skid()
    {
        transform.LookAt(transform.position + (Vector3.Scale(rigidBody.velocity, new Vector3(1, 0, 1)).normalized));
        ControlsEnabled = false;
        //rigidBody.velocity = Vector3.zero;
        yield return new WaitForSeconds(0.5f);
        ControlsEnabled = true;
        print("Skid ended");
    }

    /*Vector3 GetGroundNormal()
    {
        return groundFinder.GetGroundNormal();
    }*/
}
