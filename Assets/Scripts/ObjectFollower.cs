﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFollower : MonoBehaviour
{
    public Transform Target;
    Vector3 TargetPos;
    public Vector3 FollowOffset;
    public float MoveSpeed = 10.0f;
    public bool MatchRotation;
    public float RotationSpeed = 10.0f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, Target.position + FollowOffset, MoveSpeed * Time.fixedDeltaTime);
        if (MatchRotation)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Target.rotation, RotationSpeed * Time.fixedDeltaTime);
        }
    }
}
