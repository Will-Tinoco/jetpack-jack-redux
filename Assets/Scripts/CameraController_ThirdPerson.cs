﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController_ThirdPerson : MonoBehaviour {
    enum CameraMode { Normal, Flying };

    public float ControllerDeadZone = 0.3f;
    public float RotateSpeed = 15.0f;
    public float RotateSpeed_Mouse = 0.2f;
    public bool InvertX;
    public bool InvertY;
    CameraMode Mode = CameraMode.Normal;
    Vector3 MovementVector;
    float XRotation;
    float YRotation;
    ObjectFollower Follower;
    Quaternion TargetRotation;
    Transform MyCamera;
    Vector3 TargetRotationModifier;

	// Use this for initialization
	void Start () {
        Follower = GetComponent<ObjectFollower>();
        XRotation = transform.rotation.x;
        YRotation = transform.rotation.y;
        TargetRotation = transform.rotation;
        MyCamera = transform.Find("Main Camera");
	}
	
	// Update is called once per frame
	void Update () {
        GetInput();
	}

    private void FixedUpdate()
    {
        MoveCam();
    }

    void GetInput()
    {
        MovementVector = new Vector3();

        float _MoveHoriz = Input.GetAxis("Cam Horiz");
        float _MoveVert = Input.GetAxis("Cam Vert");

        float _MoveHoriz_Mouse = Input.GetAxis("Cam Mouse Horiz") * RotateSpeed_Mouse;
        float _MoveVert_Mouse = Input.GetAxis("Cam Mouse Vert") * RotateSpeed_Mouse;

        if (_MoveHoriz_Mouse != 0)
        {
            _MoveHoriz = _MoveHoriz_Mouse;
        }
        if (_MoveVert_Mouse != 0)
        {
            _MoveVert = _MoveVert_Mouse;
        }
        
        if (InvertX)
        {
            _MoveHoriz *= -1;
        }

        if (InvertY)
        {
            _MoveVert *= -1;
        }

        Vector3 _CamMovement = new Vector3(_MoveVert, _MoveHoriz, 0);
        //print("Getting Input");

        XRotation += _MoveVert;
        XRotation = Mathf.Clamp(XRotation, 15, 85);

        switch (Mode)
        {
            case CameraMode.Normal:
                if (Vector3.Magnitude(_CamMovement) > ControllerDeadZone)
                {
                    MovementVector = _CamMovement;
                    //print("Moving Camera");
                }
                break;
            // Vertical rotation only when flying
            case CameraMode.Flying:
                if (Vector3.Magnitude(_CamMovement) > ControllerDeadZone)
                {
                    MovementVector = new Vector3(_CamMovement.x, 0, 0);
                    //print("Moving Camera");
                }
                break;
        }
    }

    void MoveCam()
    {
        switch (Mode)
        {
            case CameraMode.Normal:
                transform.Rotate(MovementVector * RotateSpeed * Time.fixedDeltaTime);
                transform.eulerAngles = new Vector3(Mathf.Clamp(transform.eulerAngles.x, 15, 75), transform.eulerAngles.y, 0);// Mathf.Clamp(transform.eulerAngles.z, -85, 85));
                /*if(MyCamera.localEulerAngles != new Vector3())
                {
                    MyCamera.localEulerAngles = Vector3.Lerp(MyCamera.localEulerAngles, new Vector3(), Time.fixedDeltaTime * 10);
                }*/
                break;
            case CameraMode.Flying:
                transform.Rotate(MovementVector * RotateSpeed * Time.fixedDeltaTime);
                //transform.rotation = Quaternion.Euler(Vector3.Lerp(transform.eulerAngles, transform.eulerAngles + new Vector3(MovementVector.x * RotateSpeed, GetComponent<ObjectFollower>().Target.transform.eulerAngles.y - transform.eulerAngles.y, 0), Time.fixedDeltaTime * 10));
                /*if(GetComponent<ObjectFollower>().Target.transform.eulerAngles.y - transform.eulerAngles.y > 340)
                {
                    transform.eulerAngles += new Vector3(0, 360, 0);
                }*/
                TargetRotation = Quaternion.Lerp(transform.rotation, Follower.Target.transform.rotation, Time.fixedDeltaTime * 10);
                transform.rotation = TargetRotation;
                transform.eulerAngles = new Vector3(XRotation, transform.eulerAngles.y, transform.eulerAngles.z);
                //TargetRotationModifier = Quaternion.AngleAxis(transform.eulerAngles.y, Vector3.up) * TargetRotationModifier;
                //transform.rotation = Quaternion.Lerp(transform.rotation, transform.rotation * /*Quaternion.AngleAxis(TargetRotation.eulerAngles.y, Vector3.up) * */ Quaternion.Euler(TargetRotationModifier), Time.fixedDeltaTime * 10);
                //transform.eulerAngles = new Vector3(transform.eulerAngles.x + TargetRotationModifier.x, transform.eulerAngles.y, transform.eulerAngles.z + TargetRotationModifier.z);
                //transform.Rotate(MovementVector);
                //transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(transform.eulerAngles.x, GetComponent<ObjectFollower>().Target.transform.eulerAngles.y, 0), Time.fixedDeltaTime * 10);
                //transform.eulerAngles = new Vector3(Mathf.Clamp(transform.eulerAngles.x, 15, 75), transform.eulerAngles.y, 0);// Mathf.Clamp(transform.eulerAngles.z, -85, 85));
                //MyCamera.localEulerAngles = Vector3.Lerp(MyCamera.localEulerAngles, TargetRotationModifier, Time.fixedDeltaTime * 5);
                MyCamera.localEulerAngles = TargetRotationModifier;
                break;
        }
    }

    public void SetTargetRotationModifier(Vector3 _TargetRotationModifier)
    {
        TargetRotationModifier = _TargetRotationModifier;
    }

    public Vector3 GetTargetRotationModifier()
    {
        return TargetRotationModifier;
    }

    public void ChangeMode_Flying()
    {
        Mode = CameraMode.Flying;
    }

    public void ChangeMode_Normal()
    {
        Mode = CameraMode.Normal;
        MyCamera.localEulerAngles = new Vector3();
    }
}
